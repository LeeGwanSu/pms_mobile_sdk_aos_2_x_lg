package com.pms.sdk.api.request;

import java.net.URI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

public class DeviceCert extends BaseRequest {

	//	private static final boolean IS_NEW_UUID = true;
	private static final boolean IS_NEW_UUID = false;

	public DeviceCert(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam () {
		JSONObject jobj;

		try {
			jobj = new JSONObject();

			// new version
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
			jobj.put("uuid", PMSUtil.getUUID(mContext));
			jobj.put("pushToken", PMSUtil.getGCMToken(mContext));
			jobj.put("custId", PMSUtil.getCustId(mContext));
			jobj.put("appVer", PhoneState.getAppVersion(mContext));
			jobj.put("os", "A");
			jobj.put("osVer", PhoneState.getOsVersion());
			jobj.put("device", PhoneState.getDeviceName());
			jobj.put("mktVer", FLAG_Y);
			jobj.put("sessCnt", "1");

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {

		// pushToken이 set되기 전까지 sleep이 있어야 하기 때문에, 다른 api와 달리 AsyncTask로 만듬
		new AsyncTask<String, Object, Boolean>() {
			@Override
			protected Boolean doInBackground (String... params) {
				try {
					String custId = PMSUtil.getCustId(mContext);

					// 새로운 발급 방법일 경우에만 UUID 미리 발급
					boolean isNExistsUUID = PhoneState.createDeviceToken(mContext);
					CLog.i("Device Cert request get UUID : " + (isNExistsUUID == true ? "Created" : "Exists"));

					// 앱이 처음 수행 됐을 때, push token을 받아오기 전 까지 device cert를 수행하지 않게함
					int checkCnt = 0;
					while (checkCnt < 15 && (StringUtil.isEmpty(PMSUtil.getGCMToken(mContext)) || NO_TOKEN.equals(PMSUtil.getGCMToken(mContext)))) {
						try {
							CLog.i("deviceCert:pushToken is null, sleep(2000)");
							checkCnt++;
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

					if (StringUtil.isEmpty(PMSUtil.getGCMToken(mContext))) {
						// check push token
						CLog.i("DeviceCert:no push token");
						mPrefs.putString(KEY_GCM_TOKEN, NO_TOKEN);
					}

					CLog.i("DeviceCert:validate ok");

					apiManager.call(API_DEVICE_CERT, getParam(), new APICallback() {
						@Override
						public void response (String code, JSONObject json) {
							if (CODE_SUCCESS.equals(code)) {
								PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
								requiredResultProc(json);
							}

							publishProgress(code, json);
						}
					});

					return true;
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}

			@Override
			protected void onProgressUpdate (Object... values) {
				if (apiCallback != null) {
					apiCallback.response((String) values[0], (JSONObject) values[1]);
				}
			};

		}.execute();
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	@SuppressLint("DefaultLocale")
	private boolean requiredResultProc (JSONObject json) {
		try {
			PMSUtil.setAppUserId(mContext, json.getString("appUserId"));
			PMSUtil.setEncKey(mContext, json.getString("encKey"));

			// set msg flag
			mPrefs.putString(PREF_API_LOG_FLAG, json.getString("collectApiLogFlag"));
			mPrefs.putString(PREF_PRIVATE_LOG_FLAG, json.getString("collectPrivateLogFlag"));
			String custId = PMSUtil.getCustId(mContext);
			if (!StringUtil.isEmpty(custId)) {
				mPrefs.putString(PREF_LOGINED_CUST_ID, PMSUtil.getCustId(mContext));
			}

			// set config flag
			final String mflag = json.getString("msgFlag");
			final String nflag = json.getString("notiFlag");
			final String nflag1 = json.getString("notiFlag1");
			final String nflag2 = json.getString("notiFlag2");
			final String nflag3 = json.getString("notiFlag3");
			final String nflag4 = json.getString("notiFlag4");
			final String nflag5 = json.getString("notiFlag5");
			final String nflag6 = json.getString("notiFlag6");
			final String mkflag = json.getString("mktFlag");

			if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
				mPrefs.putString(PREF_MSG_FLAG, mflag);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
				mPrefs.putString(PREF_NOTI_FLAG, nflag);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG1))) {
				mPrefs.putString(PREF_NOTI_FLAG1, nflag1);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG2))) {
				mPrefs.putString(PREF_NOTI_FLAG2, nflag2);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG3))) {
				mPrefs.putString(PREF_NOTI_FLAG3, nflag3);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG4))) {
				mPrefs.putString(PREF_NOTI_FLAG4, nflag4);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG5))) {
				mPrefs.putString(PREF_NOTI_FLAG5, nflag5);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG6))) {
				mPrefs.putString(PREF_NOTI_FLAG6, nflag6);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_MKT_FLAG))) {
				mPrefs.putString(PREF_MKT_FLAG, mkflag);
			}

			if ((mPrefs.getString(PREF_MSG_FLAG).equals(mflag) == false) || (mPrefs.getString(PREF_NOTI_FLAG).equals(nflag) == false)
					|| (mPrefs.getString(PREF_NOTI_FLAG1).equals(nflag1) == false) || (mPrefs.getString(PREF_NOTI_FLAG2).equals(nflag2) == false)
					|| (mPrefs.getString(PREF_NOTI_FLAG3).equals(nflag3) == false) || (mPrefs.getString(PREF_NOTI_FLAG4).equals(nflag4) == false)
					|| (mPrefs.getString(PREF_NOTI_FLAG5).equals(nflag5) == false) || (mPrefs.getString(PREF_NOTI_FLAG6).equals(nflag6) == false)
					|| (mPrefs.getString(PREF_MKT_FLAG).equals(mkflag) == false)) {
				new SetConfig(mContext).request(mflag, nflag, nflag1, nflag2, nflag3, nflag4, nflag5, nflag6, mkflag, null);
			}

			// set badge
			Badge.getInstance(mContext).updateBadge(json.getString("newMsgCnt"));

			// readMsg
			JSONArray readArray = PMSUtil.arrayFromPrefs(mContext, PREF_READ_LIST);
			if (readArray.length() > 0) {
				// call readMsg
				new ReadMsg(mContext).request(readArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete readMsg
							new Prefs(mContext).putString(PREF_READ_LIST, "");
						}
					}
				});
			} else {
				CLog.i("readArray is null");
			}

			// clickMsg
			JSONArray clickArray = PMSUtil.arrayFromPrefs(mContext, PREF_CLICK_LIST);
			if (clickArray.length() > 0) {
				// call clickMsg
				new ClickMsg(mContext).request(clickArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete clickMsg
							new Prefs(mContext).putString(PREF_CLICK_LIST, "");
						}
					}
				});
			} else {
				CLog.i("clickArray is null");
			}

			// mqttFlag Y/N
			if (FLAG_Y.equals(PMSUtil.getMQTTFlag(mContext))) {
				String flag = json.getString("privateFlag");
				mPrefs.putString(PREF_PRIVATE_FLAG, flag);
				if (FLAG_Y.equals(flag)) {
					String protocol = json.getString("privateProtocol");
					String protocolTemp = "";
					try {
						URI uri = new URI(PMSUtil.getMQTTServerUrl(mContext));
						if (protocol.equals(PROTOCOL_TCP)) {
							protocolTemp = protocol.toLowerCase() + "cp";
						} else if (protocol.equals(PROTOCOL_SSL)) {
							protocolTemp = protocol.toLowerCase() + "sl";
						}

						if (uri.getScheme().equals(protocolTemp)) {
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
						} else {
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
							mContext.stopService(new Intent(mContext, MQTTService.class));
						}
					} catch (NullPointerException e) {
						mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
					}
					mContext.sendBroadcast(new Intent(mContext, RestartReceiver.class));
				} else {
					mContext.stopService(new Intent(mContext, MQTTService.class));
				}
			}

			// LogFlag Y/N
			if ((FLAG_N.equals(mPrefs.getString(PREF_API_LOG_FLAG)) && FLAG_N.equals(mPrefs.getString(PREF_PRIVATE_LOG_FLAG))) == false) {
				setCollectLog();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void setCollectLog () {
		boolean isAfter = false;
		String beforeDate = mPrefs.getString(PREF_YESTERDAY);
		String today = DateUtil.getNowDateMo();

		try {
			isAfter = DateUtil.isDateAfter(beforeDate, today);
		} catch (Exception e) {
			isAfter = false;
		}

		if (isAfter) {
			mPrefs.putBoolean(PREF_ONEDAY_LOG, false);
		}

		if (mPrefs.getBoolean(PREF_ONEDAY_LOG) == false) {
			if (beforeDate.equals("")) {
				beforeDate = DateUtil.getNowDateMo();
				mPrefs.putString(PREF_YESTERDAY, beforeDate);
			}
			new CollectLog(mContext).request(beforeDate, null);
		}
	}
}
