package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.PMSUtil;

public class GetConfig extends BaseRequest {

	public GetConfig(Context context) {
		super(context);
	}

	public JSONObject getParam () {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("loginState", mPrefs.getString(PREF_LOGINED_STATE));
			jobj.put("custId", PMSUtil.getCustId(mContext));
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			apiManager.call(API_GET_CONFIG, getParam(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			mPrefs.putString(PREF_MSG_FLAG, json.getString("msgFlag"));
			mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
			mPrefs.putString(PREF_NOTI_FLAG1, json.getString("notiFlag1"));
			mPrefs.putString(PREF_NOTI_FLAG2, json.getString("notiFlag2"));
			mPrefs.putString(PREF_NOTI_FLAG3, json.getString("notiFlag3"));
			mPrefs.putString(PREF_NOTI_FLAG4, json.getString("notiFlag4"));
			mPrefs.putString(PREF_NOTI_FLAG5, json.getString("notiFlag5"));
			mPrefs.putString(PREF_NOTI_FLAG6, json.getString("notiFlag6"));
			mPrefs.putString(PREF_MKT_FLAG, json.getString("mktFlag"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
