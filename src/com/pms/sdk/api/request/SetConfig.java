package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;

public class SetConfig extends BaseRequest {

	public SetConfig(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @param msgFlag
	 * @param notiFlag
	 * @return
	 */
	public JSONObject getParam (String msgFlag, String notiFlag, String noti_flag1, String noti_flag2, String noti_flag3, String noti_flag4,
			String noti_flag5, String noti_flag6, String mktFlag) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("msgFlag", msgFlag);
			jobj.put("notiFlag", notiFlag);
			jobj.put("notiFlag1", noti_flag1);
			jobj.put("notiFlag2", noti_flag2);
			jobj.put("notiFlag3", noti_flag3);
			jobj.put("notiFlag4", noti_flag4);
			jobj.put("notiFlag5", noti_flag5);
			jobj.put("notiFlag6", noti_flag6);
			jobj.put("mktFlag", mktFlag);
			jobj.put("mktVer", FLAG_Y);
			jobj.put("loginState", mPrefs.getString(PREF_LOGINED_STATE));
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String msgFlag, String notiFlag, String noti_flag1, String noti_flag2, String noti_flag3, String noti_flag4,
			String noti_flag5, String noti_flag6, String mktFlag, final APICallback apiCallback) {
		try {
			apiManager.call(API_SET_CONFIG,
					getParam(msgFlag, notiFlag, noti_flag1, noti_flag2, noti_flag3, noti_flag4, noti_flag5, noti_flag6, mktFlag), new APICallback() {
						@Override
						public void response (String code, JSONObject json) {
							if (CODE_SUCCESS.equals(code)) {
								requiredResultProc(json);
							}
							if (apiCallback != null) {
								apiCallback.response(code, json);
							}
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			mPrefs.putString(PREF_MSG_FLAG, json.getString("msgFlag"));
			mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
			mPrefs.putString(PREF_NOTI_FLAG1, json.getString("notiFlag1"));
			mPrefs.putString(PREF_NOTI_FLAG2, json.getString("notiFlag2"));
			mPrefs.putString(PREF_NOTI_FLAG3, json.getString("notiFlag3"));
			mPrefs.putString(PREF_NOTI_FLAG4, json.getString("notiFlag4"));
			mPrefs.putString(PREF_NOTI_FLAG5, json.getString("notiFlag5"));
			mPrefs.putString(PREF_NOTI_FLAG6, json.getString("notiFlag6"));
			mPrefs.putString(PREF_MKT_FLAG, json.getString("mktFlag"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
