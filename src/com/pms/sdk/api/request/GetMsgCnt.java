package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;

public class GetMsgCnt extends BaseRequest {

	public GetMsgCnt(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String msgId) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("msgId", msgId);

			if (!StringUtil.isEmpty(PMSUtil.getCustId(mContext))) {
				jobj.put("custId", PMSUtil.getCustId(mContext));
			}

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param custId
	 * @param userData
	 * @param apiCallback
	 */
	public void request (String msgId, final APICallback apiCallback) {
		try {
			apiManager.call(API_GET_MSG_CNT, getParam(msgId), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			String cnt = json.getString("msgCnt");
			int index = cnt.indexOf("/");
			mPrefs.putInt(PREF_UNREAD_CNT, Integer.valueOf(cnt.substring(0, index)));
			mPrefs.putInt(PREF_TOTAL_CNT, Integer.valueOf(cnt.substring((index + 1), cnt.length())));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return true;
	}
}
