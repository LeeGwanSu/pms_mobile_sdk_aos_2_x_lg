package com.pms.sdk.push;

import java.lang.reflect.Field;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.Volley;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.QueueManager;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.view.BitmapLruCache;

/**
 * push receiver
 * 
 * @author erzisk
 * @since 2013.06.07
 */
public class PushReceiver extends BroadcastReceiver implements IPMSConsts {

	private static final int START_TASK_TO_FRONT = 2;

	private static final String USE = "Y";

		private Prefs mPrefs;

	private PowerManager pm;
	private PowerManager.WakeLock wl;

	private static final int DEFAULT_SHOWING_TIME = 30000;

	private final Handler mFinishHandler = new Handler();

	private Bitmap mPushImage;

	/**
	 * gcm key register
	 * 
	 * @param context
	 * @param senderId
	 */
	public static void gcmRegister (Context context, String senderId) {
		Intent registrationIntent = new Intent(ACTION_REGISTER);
		registrationIntent.setPackage(GSF_PACKAGE);
		registrationIntent.putExtra(KEY_APP, PendingIntent.getBroadcast(context, 0, new Intent(), 0));
		registrationIntent.putExtra(KEY_SENDER, senderId);
		context.startService(registrationIntent);
	}

	@Override
	public synchronized void onReceive (final Context context, final Intent intent) {
		CLog.i("onReceive() -> " + intent.toString());
		mPrefs = new Prefs(context);

		if (ACTION_REGISTRATION.equals(intent.getAction())) {
			// registration
			CLog.i("onReceive:registration");
			if (intent.getStringExtra(KEY_GCM_TOKEN) != null) {
				// regist gcm key
				CLog.d("handleRegistration:key=" + intent.getStringExtra(KEY_GCM_TOKEN));
				mPrefs.putString(KEY_GCM_TOKEN, intent.getStringExtra(KEY_GCM_TOKEN));
			} else {
				// error occurred
				CLog.i("handleRegistration:error=" + intent.getStringExtra("error"));
				CLog.i("handleRegistration:unregistered=" + intent.getStringExtra("unregistered"));
			}
		} else {
			// receive push message
			if (MQTTService.INTENT_RECEIVED_MSG.equals(intent.getAction())) {
				// private server
				String message = intent.getStringExtra(MQTTService.KEY_MSG);

				CLog.i("onReceive:receive from private server");

				// set push info
				try {
					JSONObject msgObj = new JSONObject(message);
					try {
						intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (intent.getAction().equals(ACTION_RECEIVE)) {
				context.sendBroadcast(new Intent(ACTION_FORCE_START).putExtras(intent));
				// gcm
				// key: title, msgType, message, sound, data
				CLog.i("onReceive:receive from GCM");
			}

			if (isImagePush(intent.getExtras())) {
				try {
					// image push
					QueueManager queueManager = QueueManager.getInstance();
					RequestQueue queue = queueManager.getRequestQueue();
					ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());

					imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener() {
						@Override
						public void onResponse (ImageContainer response, boolean isImmediate) {
							if (response == null) {
								CLog.e("response is null");
								return;
							}
							if (response.getBitmap() == null) {
								CLog.e("bitmap is null");
								return;
							}
							mPushImage = response.getBitmap();
							CLog.i("imageWidth:" + mPushImage.getWidth());
							onMessage(context, intent);
						}

						@Override
						public void onErrorResponse (VolleyError error) {
							CLog.e("onErrorResponse:" + error.getMessage());
							// wrong img url (or exception)
							onMessage(context, intent);
						}
					});
				} catch (NullPointerException e) {
					e.printStackTrace();
				}

			} else {
				// default push
				onMessage(context, intent);
			}
		}
	}

	/**
	 * on message (gcm, private msg receiver)
	 * 
	 * @param context
	 * @param intent
	 */
	private synchronized void onMessage (final Context context, Intent intent) {

		final Bundle extras = intent.getExtras();

		PMS pms = PMS.getInstance(context);

		PushMsg pushMsg = new PushMsg(extras);

		if (StringUtil.isEmpty(pushMsg.msgId) || StringUtil.isEmpty(pushMsg.notiTitle) || StringUtil.isEmpty(pushMsg.notiMsg)
				|| StringUtil.isEmpty(pushMsg.msgType)) {
			CLog.i("msgId or notiTitle or notiMsg or msgType is null");
			return;
		}

		CLog.i(pushMsg + "");

		PMSDB db = PMSDB.getInstance(context);

		// check already exist msg
		Msg existMsg = db.selectMsgWhereMsgId(pushMsg.msgId);
		if (existMsg != null && existMsg.msgId.equals(pushMsg.msgId)) {
			CLog.i("already exist msg");
			return;
		}

		// insert (temp) new msg
		Msg newMsg = new Msg();
		newMsg.readYn = Msg.READ_N;
		newMsg.msgGrpCd = "999999";
		newMsg.expireDate = "0";
		newMsg.msgId = pushMsg.msgId;

		db.insertMsg(newMsg);

		// refresh list and badge
		context.sendBroadcast(new Intent(RECEIVER_PUSH).putExtras(extras));

		// show noti
		// ** 수정 ** 기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함
		CLog.i("NOTI FLAG : " + mPrefs.getString(PREF_NOTI_FLAG));

		if (USE.equals(mPrefs.getString(PREF_NOTI_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
			// check push flag

			if (USE.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
				// screen on
				pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Tag");
				if (!pm.isScreenOn()) {
					wl.acquire();
					mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
				}
			}

			CLog.i("version code :" + Build.VERSION.SDK_INT);

			// execute push noti listener
			if (pms.getOnReceivePushListener() != null) {
				if (pms.getOnReceivePushListener().onReceive(context, extras)) {
					showNotification(context, extras);
				}
			} else {
				showNotification(context, extras);
			}

			CLog.i("ALERT FLAG : " + mPrefs.getString(PREF_ALERT_FLAG));

			if (USE.equals(mPrefs.getString(PREF_ALERT_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
				showPopup(context, extras);
			}
		}
	}

	/**
	 * show notification
	 * 
	 * @param context
	 * @param extras
	 */
	private synchronized void showNotification (Context context, Bundle extras) {

		// push intent sample
		// String msgId = extras.getString(KEY_MSG_ID);
		// String title = extras.getString(KEY_TITLE);
		// String msg = extras.getString(KEY_MSG);
		// String msgType = extras.getString(KEY_MSG_TYPE);
		// String sound = extras.getString(KEY_SOUND);
		// String data = extras.getString(KEY_DATA);
		CLog.i("showNotification");
		if (isImagePush(extras)) {
			showNotificationImageStyle(context, extras);
		} else {
			showNotificationTextStyle(context, extras);
		}
	}

	/**
	 * show notification text style
	 * 
	 * @param context
	 * @param extras
	 */
	private synchronized void showNotificationTextStyle (Context context, Bundle extras) {
		CLog.i("showNotificationTextStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder;

		// Notification channel added
//		if (PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1 &&
		int nNotiChannel = mPrefs.getInt(PREF_NCHANNEL_ID);
		nNotiChannel = nNotiChannel == -1 ? 0 : nNotiChannel;
		if ( Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
			
			NotificationChannel notiChannel = notificationManager.getNotificationChannel(nNotiChannel + "");

			if (notiChannel == null) {
				notiChannel = new NotificationChannel(nNotiChannel + "",
						PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
				notiChannel.setShowBadge(mPrefs.getBoolean(IPMSConsts.PREF_NCHANNEL_SHOW_BADGE_FLAG));
			}

			if (notificationManager.getNotificationChannel(NOTIFICATION_ID + "") != null) {
				CLog.d("deleteNotificationChannel : " + NOTIFICATION_ID);
				notificationManager.deleteNotificationChannel(NOTIFICATION_ID + "");
			}
			
			boolean boolShowBadgeSetting = mPrefs.getBoolean(PREF_NCHANNEL_SHOW_BADGE_FLAG);
			CLog.d("notiChannel.canShowBadge() : " + notiChannel.canShowBadge());
			if ((!notiChannel.canShowBadge() && boolShowBadgeSetting) ||
					(notiChannel.canShowBadge() && !boolShowBadgeSetting)) {
				CLog.d("showNotificationTextStyle checkChannel");
				checkChannel(notificationManager, boolShowBadgeSetting, nNotiChannel +"",
						++nNotiChannel +"", PMSUtil.getApplicationName(context) );
				mPrefs.putInt(PREF_NCHANNEL_ID, nNotiChannel);
			}
			else {
				CLog.d("showNotificationTextStyle createNotificationChannel");
				notificationManager.createNotificationChannel(notiChannel);
			}
			
		}
		
		builder = new NotificationCompat.Builder(context, nNotiChannel + "");
		builder.setNumber(mPrefs.getInt(PREF_NCHANNEL_BADGE_NUMBER));
		builder.setContentIntent(makePendingIntent(context, extras));
		builder.setAutoCancel(true);
		builder.setContentText(pushMsg.notiMsg);
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);

		int iconId = 0;
		int largeIconId = 0;
		int ver = Build.VERSION.SDK_INT;
		if (ver >= Build.VERSION_CODES.LOLLIPOP) {
			iconId = PMSUtil.getIconId(context);
			largeIconId = PMSUtil.getLargeIconId(context);
			builder.setColor(Color.parseColor(PMSUtil.getNotiBackColor(context)));
		} else {
			iconId = PMSUtil.getLargeIconId(context);
		}

		// set small icon
		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

		// checek ring mode
		if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
			if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
				try {
					int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
					if (notiSound > 0) {
						Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
						builder.setSound(soundUri);
					} else {
						throw new Exception("DEFAULT_SOUND");
					}
				} catch (Exception e) {
					Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

					builder.setSound(uri);
				}
			}
		}

		// check vibe mode
		if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			builder.setDefaults(Notification.DEFAULT_VIBRATE);
		}

		if (FLAG_Y.equals(mPrefs.getString(PREF_USE_BIGTEXT))) {
			builder.setStyle(new NotificationCompat.BigTextStyle()
					.setBigContentTitle(pushMsg.notiTitle).bigText(pushMsg.notiMsg));
		}

		// show notification
		notificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	/**
	 * show notification image style
	 * 
	 * @param context
	 * @param extras
	 */
	@SuppressLint("NewApi")
	private synchronized void showNotificationImageStyle (Context context, Bundle extras) {
		CLog.i("showNotificationImageStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification.Builder builder;
		
		if ( Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
			int nNotiChannel = mPrefs.getInt(PREF_NCHANNEL_ID);
			CLog.d("prev nNotiChannel : " + nNotiChannel);
			nNotiChannel = nNotiChannel == -1 ? 0 : nNotiChannel;
			NotificationChannel notiChannel = notificationManager.getNotificationChannel(nNotiChannel + "");

			if (notiChannel == null) {
				notiChannel = new NotificationChannel(nNotiChannel + "",
						PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
				notiChannel.setShowBadge(mPrefs.getBoolean(IPMSConsts.PREF_NCHANNEL_SHOW_BADGE_FLAG));
			}

			if (notificationManager.getNotificationChannel(NOTIFICATION_ID + "") != null) {
				CLog.d("deleteNotificationChannel : " + NOTIFICATION_ID);
				notificationManager.deleteNotificationChannel(NOTIFICATION_ID + "");
			}
			
			boolean boolShowBadgeSetting = mPrefs.getBoolean(PREF_NCHANNEL_SHOW_BADGE_FLAG);
			CLog.d("notiChannel.canShowBadge() : " + notiChannel.canShowBadge());
			if ((!notiChannel.canShowBadge() && boolShowBadgeSetting) ||
					(notiChannel.canShowBadge() && !boolShowBadgeSetting)) {
				CLog.d("showNotificationImageStyle checkChannel");
				checkChannel(notificationManager, boolShowBadgeSetting, nNotiChannel +"",
						++nNotiChannel +"", PMSUtil.getApplicationName(context) );
				mPrefs.putInt(PREF_NCHANNEL_ID, nNotiChannel);
			}
			else {
				CLog.d("showNotificationImageStyle createNotificationChannel");
				notificationManager.createNotificationChannel(notiChannel);
			}
			
			builder = new Notification.Builder(context, nNotiChannel + "");
		}
		else {
			builder = new Notification.Builder(context);
		}

//		builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		builder.setNumber(mPrefs.getInt(PREF_NCHANNEL_BADGE_NUMBER));
		builder.setContentIntent(makePendingIntent(context, extras));
		builder.setAutoCancel(true);
		builder.setContentText(PMSUtil.getBigNotiContextMsg(context));
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		if (PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1 &&
				Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {

		} else {
			builder.setLights(0xFF00FF00, 1000, 2000);
		}

		int iconId = 0;
		int largeIconId = 0;
		int ver = Build.VERSION.SDK_INT;
		if (ver >= Build.VERSION_CODES.LOLLIPOP) {
			iconId = PMSUtil.getIconId(context);
			largeIconId = PMSUtil.getLargeIconId(context);
			builder.setColor(Color.parseColor(PMSUtil.getNotiBackColor(context)));
		} else {
			iconId = PMSUtil.getLargeIconId(context);
		}

		// set small icon
		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

		// checek ring mode
		if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
			if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
				try {
					int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
					if (notiSound > 0) {
						Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
						builder.setSound(soundUri);
					} else {
						throw new Exception("DEFAULT_SOUND");
					}
				} catch (Exception e) {
					Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

					builder.setSound(uri);
				}
			}
		}

		// check vibe mode
		if (PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1 &&
				Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {

		} else {
			// check vibe mode
			if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
				builder.setDefaults(Notification.DEFAULT_VIBRATE);
			}
		}

		// show notification
		builder.setStyle(new Notification.BigPictureStyle(builder).bigPicture(mPushImage).setBigContentTitle(pushMsg.notiTitle)
				.setSummaryText(pushMsg.notiMsg));
		notificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	@TargetApi(Build.VERSION_CODES.O)
	private static void checkChannel(NotificationManager notificationManager, boolean isShow, 
									 String oldChId, String newChId, String chName) {
		try {
			if (oldChId != null && notificationManager.getNotificationChannel(oldChId) != null) {
				notificationManager.deleteNotificationChannel(oldChId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if (newChId != null && chName != null) {
				NotificationChannel notiChannel = new NotificationChannel(
						newChId, chName, NotificationManager.IMPORTANCE_HIGH);
				
				if (isShow) {
					notiChannel.setShowBadge(true);
					CLog.d("setShowBadge true");
				}
				else {
					notiChannel.setShowBadge(false);
					CLog.d("setShowBadge false");
				}
				
				notificationManager.createNotificationChannel(notiChannel);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * make pending intent
	 * 
	 * @param context
	 * @param extras
	 * @return
	 */
	private PendingIntent makePendingIntent (Context context, Bundle extras) {
		// notification
		Intent innerIntent = null;
		String receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER_CLASS);

		if (receiverClass != null) {
			try {
				Class<?> cls = Class.forName(receiverClass);
				innerIntent = new Intent(context, cls).putExtras(extras);
			} catch (ClassNotFoundException e) {
				CLog.e(e.getMessage());
			}
		}
		if (innerIntent == null) {
			CLog.d("innerIntent == null");
			receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER);
			receiverClass = receiverClass != null ? receiverClass : "com.pms.sdk.notification";

			// setting push info to intent
			innerIntent = new Intent(receiverClass).putExtras(extras);
		}
		return PendingIntent.getBroadcast(context, 0, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	/**
	 * show popup (activity)
	 * 
	 * @param context
	 * @param extras
	 */
	@SuppressLint("DefaultLocale")
	private synchronized void showPopup (Context context, Bundle extras) {
		try {
			Class<?> pushPopupActivity;

			String pushPopupActivityName = mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY);

			if (StringUtil.isEmpty(pushPopupActivityName)) {
				pushPopupActivityName = DEFAULT_PUSH_POPUP_ACTIVITY;
			}

			try {
				pushPopupActivity = Class.forName(pushPopupActivityName);
			} catch (ClassNotFoundException e) {
				CLog.e(e.getMessage());
				pushPopupActivity = PushPopupActivity.class;
			}
			CLog.i("pushPopupActivity :" + pushPopupActivityName);

			Intent pushIntent = new Intent(context, pushPopupActivity);
			pushIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION
					| Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
			pushIntent.putExtras(extras);

			if (isOtherApp(context)) {
				context.startActivity(pushIntent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "deprecation", "static-access" })
	@SuppressLint("DefaultLocale")
	private boolean isOtherApp (Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
		String topActivity = "";

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
			RunningAppProcessInfo currentInfo = null;
			Field field = null;
			try {
				field = RunningAppProcessInfo.class.getDeclaredField("processState");
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}

			List<RunningAppProcessInfo> appList = am.getRunningAppProcesses();
			for (RunningAppProcessInfo app : appList) {
				if (app.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
					Integer state = null;
					try {
						state = field.getInt(app);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
					if (state != null && state == START_TASK_TO_FRONT) {
						currentInfo = app;
						break;
					}
				}
			}
			topActivity = currentInfo.processName;
		} else {
			List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(10);
			topActivity = taskInfo.get(0).topActivity.getPackageName();
		}

		CLog.e("TOP Activity : " + topActivity);
		if (topActivity.equals(context.getPackageName())) {
			return true;
		}
		return false;
	}

	/**
	 * is image push
	 * 
	 * @param extras
	 * @return
	 */
	@SuppressLint("DefaultLocale")
	private boolean isImagePush (Bundle extras) {
		try {
			if (!PhoneState.isNotificationNewStyle()) {
				throw new Exception("wrong os version");
			}
			String notiImg = extras.getString(KEY_NOTI_IMG);
			CLog.i("notiImg:" + notiImg);
			if (notiImg == null || "".equals(notiImg) || (notiImg.indexOf("http") == -1)) {
				throw new Exception("no image type");
			}
			return true;
		} catch (Exception e) {
			CLog.e("isImagePush:" + e.getMessage());
			return false;
		}
	}

	/**
	 * finish runnable
	 */
	private final Runnable finishRunnable = new Runnable() {
		@Override
		public void run () {
			if (wl != null && wl.isHeld()) {
				wl.release();
			}
		};
	};

	/**
	 * show instant view (toast)
	 * 
	 * @param context
	 * @param msg
	 */
	// private void showInstantView(Context context, String msg) {
	// LayoutInflater mInflater = LayoutInflater.from(context);
	// View v = mInflater.inflate(R.layout.pms_push_instant_view, null);
	// LinearLayout layPushMsg = (LinearLayout) v.findViewById(R.id.pms_lay_push_msg);
	// TextView txtPushTitle = (TextView) v.findViewById(R.id.pms_txt_push_title);
	// TextView txtPushMsg = (TextView) v.findViewById(R.id.pms_txt_push_msg);
	//
	// txtPushMsg.setText(msg);
	//
	// String currentTheme = Prefs.getString(context, Consts.PREF_THEME);
	// if (currentTheme.equals(context.getString(R.string.pms_txt_green))) {
	// layPushMsg.setBackgroundColor(0xCCC1C95E);
	// txtPushTitle.setTextColor(0xFFC1DF7D);
	// } else if (currentTheme.equals(context.getString(R.string.pms_txt_pink))) {
	// layPushMsg.setBackgroundColor(0xCCF695CA);
	// txtPushTitle.setTextColor(0xFFFED6F0);
	// } else if (currentTheme.equals(context.getString(R.string.pms_txt_brown))) {
	// layPushMsg.setBackgroundColor(0xCCD09F76);
	// txtPushTitle.setTextColor(0xFFFDD9B9);
	// } else if (currentTheme.equals(context.getString(R.string.pms_txt_black))) {
	// layPushMsg.setBackgroundColor(0xCC5C5C5C);
	// txtPushTitle.setTextColor(0xFF949494);
	// } else {
	// layPushMsg.setBackgroundColor(0xCCC1C95E);
	// txtPushTitle.setTextColor(0xFFC1DF7D);
	// }
	//
	// txtPushMsg.setTextColor(0xFFFFFFFF);
	//
	// Toast toast = new Toast(context);
	// toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
	// toast.setDuration(Toast.LENGTH_SHORT);
	// toast.setView(v);
	// toast.show();
	// }
}
