package com.pms.sdk;

import java.io.Serializable;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.PushReceiver;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version [2013.10.07] pushMsg.java 생성 - push 데이터 key 변경<br>
 *          [2013.10.11] android-query -> volley 로 library 변경<br>
 *          [2013.10.14] push notification 출력 변경<br>
 *          [2013.10.16 15:09] logout시에 저장된 cust_id삭제<br>
 *          [2013.10.21 10:30] api call시에, appUserId 또는 encKey가 없을 시 에러 추가<br>
 *          [2013.10.21 11:22] api call시에, paging이 존재하는 api(newMsg.m)일 경우, 가장 마지막 호출에서만 apiCallback이 이뤄지게끔 수정<br>
 *          [2013.10.21 18:43] push 데이터 key 변경 (IPMSConsts)<br>
 *          [2013.10.25 14:15] deviceCert시 token이 없는경우 'noToken'으로 넘김<br>
 *          [2013.10.29 17:03] deviceCert시 token이 "noToken"일 경우도 loop<br>
 *          [2013.10.30 11:26] push 수신 시 notImg exception 처리 추가<br>
 *          [2013.11.01 11:15] CLog 새로운 버전으로 적용 및 소스에서 TAG 삭제!!!!<br>
 *          [2013.11.06 15:02] deviceCert parameter에 sessCnt추가<br>
 *          [2013.11.06 16:07] logined_cust_id 추가, deviceCert 및 loginPms시에 cust_id와 logined_cust_id를 비교하여 db delete 결정<br>
 *          [2013.11.15 10:38] APIManager에서 error발생 시에도 apiCalback수행<br>
 *          [2013.11.19 15:36] push popup finish thread 추가<br>
 *          [2013.11.26 09:22] click 관련 추가<br>
 *          [2013.11.26 16:42] MQTT Client 변경<br>
 *          [2013.11.26 21:13] deviceCert시 read 및 click api 호출<br>
 *          [2013.11.27 17:51] MQTT Client 사용시 로그 저장 루틴 추가 <br>
 *          [2013.12.04 17:47] push 수신 시 해당 msgId가 존재하면 noti하지 않게 변경<br>
 *          [2013.12.05 15:35] push popup 유지 시간 설정 추가 PMS.setPushPopupShowingTime(int)<br>
 *          [2013.12.10 18:21] MQTT KeepAlive Time mata-data에서 설정하도록 수정함.<br>
 *          [2013.12.11 16:07] PMSDB.deleteExpireMsg 로직 변경<br>
 *          [2013.12.12 18:34] Private Flag 추가<br>
 *          [2013.12.16 10:33] PushReceiver.onMessage에 synchronized추가<br>
 *          [2013.12.16 16:36] Push Popup 사용자가 설정할수 있도록 변경함. <br>
 *          [2013.12.17 13:19] MQTT Client 중복 실행 수정함. <br>
 *          [2014.01.13 09:31] MQTT Client 안정화 & Connect Check 부분 수정함. <br>
 *          [2014.02.07 17:12] Collect 부분 수정함. <br>
 *          [2014.02.11 10:18] CollectLog getParam 부분 수정함. <br>
 *          [2014.02.21 17:23] MQTT Client 안정화 및 Prvate 서버 Protocol 적용 가능하게 수정함.<br>
 *          [2014.02.25 10:12] 안쓰는 method 삭제함.<br>
 *          [2014.03.03 10:36] 팝업 셋팅 default 설정 가능하게 변경.<br>
 *          [2014.03.05 15:31] MQTT Client 관련 Service 추가.<br>
 *          [2014.03.05 20:37] MQTT Client STOP 재시작하는 현상 수정.<br>
 *          [2014.03.13 10:47] MQTT Client keepAlive 수정함. sleep mode 삭제.<br>
 *          [2014.03.28 09:29] DisusePms.m API 추가함.<br>
 *          [2014.04.02 09:00] ReadMsg 변경.<br>
 *          [2014.04.15 16:27] Notification bar 이벤트 추가함. <br>
 *          [2014.04.21 11:29] setConfig Parameter 4개에 대해서 추가함. <br>
 *          [2014.05.19 16:03] setConfig parameter 2개 더 추가함. <br>
 *          [2014.05.22 11:26] DB 삭제 되는 루틴 주석. <br>
 *          [2014.05.24 08:19] newMsg호출시 msgGrpCd 변경되는 사항 수정. <br>
 *          [2014.06.12 16:09] 다른앱 사용시 팝업 노출 안되게 수정함. <br>
 *          [2014.06.16 20:49] 다른앱 실행 시 안뜨게 하는 값 추가함 (넥서스 5) <br>
 *          [2014.08.05 19:46] MQTT 버그수정 및 안정화 <br>
 *          [2014.10.21 14:52] Android OS 5.0에서 상태창 이미지 표시 안되는 버그 수정함. <br>
 *          [2014.12.22 09:48] Noti Img가 Null이나 Http로 시작하지 않을시 Text로 전환함. <br>
 *          [2015.03.13 16:37] 구글 정책에 따른 앱 실행중일떄만 팝업창 노출.<br>
 *          [2015.06.03 11:24] 버그에 대한 사항 수정함. MQTT & API쪽 라이브러리 교체함.<br>
 *          [2015.08.12 11:19] MQTT Exception 버그 사항 수정함.<br>
 *          [2016.03.17 14:04] MKT FLAG 추가함. getConfig 추가함.<br>
 *          [2016.04.29 10:04] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.05.12 18:01] CheckSelfPermission 코드 삭제함.<br>
 *          [2016.07.05 10:57] 신규 API 추가 및 mktVer 플래그 추가함.<br>
 *          [2016.07.06 14:38] Icon에 대한 버그 사항 수정함.<br>
 *          [2016.09.08 10:31] getConfig -> GetConfig로 소문자에서 대문자로 변경함.<br>
 *          [2016.09.27 14:18] GetMsgCnt에 CustID를 추가함.(로그인 했을 경우에만)<br>
 *          [2016.11.29 09:45] doze mode broadcast 패치1 적용 <br>
 *          [2017.05.23 17:14] setBigText 함수 추가 (캐리지리턴 적용)
 *          [2017.10.27 14:29] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					* DB 중복 Open으로 인해 강제종료 문제 수정
 *          					* PushPopup 발생 시 강제 종료 문제 수정
 *          					* UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					  UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함
 *         	[2018.02.26 11:14] * UUID 발급 방법을 IS_NEW_UUID flag를 통해 구분 (DeviceCert의 getParams)
 *         						Android O(8.0) 이상으로 TargetVersion 설정 후 빌드할 경우에 Notify할 시에 Channel로 notify하지 않아 강제종료 발생 문제 수정
 *         	[2018.04.04 09:26] * Notification Channel로 생성하여 8.0에서는 문제 발생 가능성 존재 (Badge counter가 setNumber에 따라서 갱신됨.
 *         						LG에서는 Badge 출력 여부를 결정할 수 있게 해달라는 요청이 있어 수정 전달 <br>	
 */
public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;
	private static PMS instancePms;
	private static PMSPopup instancePmsPopup;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:201804040926");

		initOption(context);
	}

	/**
	 * initialize option
	 */
	private void initOption (Context context) {
		if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
			mPrefs.putString(PREF_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			mPrefs.putString(PREF_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
			mPrefs.putString(PREF_ALERT_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_LOGINED_STATE))) {
			mPrefs.putString(PREF_LOGINED_STATE, FLAG_N);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
			mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
		}
		if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
			PMSUtil.setServerUrl(context, API_SERVER_URL);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
			mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
		}
		if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
			mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
		}
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @param projectId
	 * @return
	 */
	public static PMS getInstance (Context context, String projectId) {
		PMSUtil.setGCMProjectId(context, projectId);
		return getInstance(context);
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @return
	 */
	public static PMS getInstance (Context context) {
		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		if (instancePms == null) {
			instancePms = new PMS(context);
			if (PhoneState.isAvailablePush()) {
				PushReceiver.gcmRegister(context, PMSUtil.getGCMProjectId(context));
			}
		}
		instancePms.setmContext(context);
		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

	/**
	 * start mqtt service
	 */
	public void startMQTTService (Context context) {
		if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
			context.sendBroadcast(new Intent(context, RestartReceiver.class));
		} else {
			context.stopService(new Intent(context, MQTTService.class));
		}
	}

	public void stopMQTTService (Context context) {
		context.stopService(new Intent(context, MQTTService.class));
	}

	/**
	 * clear
	 * 
	 * @return
	 */
	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms.unregisterReceiver();
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	private void unregisterReceiver () {
		Badge.getInstance(mContext).unregisterReceiver();
	}

	// /**
	// * push token 세팅
	// * @param pushToken
	// */
	// public void setPushToken(String pushToken) {
	// CLog.i(TAG + "setPushToken");
	// CLog.d(TAG + "setPushToken:pushToken=" + pushToken);
	// PMSUtil.setGCMToken(mContext, pushToken);
	// }

	public void setPopupSetting (Boolean state, String title) {
		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
	}

	/**
	 * set ServerUrl
	 * 
	 * @param serverUrl
	 */
	public void setServerUrl (String serverUrl) {
		CLog.i("setServerUrl");
		CLog.d("setServerUrl:serverUrl=" + serverUrl);
		PMSUtil.setServerUrl(mContext, serverUrl);
	}

	/**
	 * cust id 세팅
	 * 
	 * @param custId
	 */
	public void setCustId (String custId) {
		CLog.i("setCustId");
		CLog.d("setCustId:custId=" + custId);
		PMSUtil.setCustId(mContext, custId);
	}

	/**
	 * get cust id
	 * 
	 * @return
	 */
	public String getCustId () {
		CLog.i("getCustId");
		return PMSUtil.getCustId(mContext);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	/**
	 * setUseBigText
	 * BigText 사용/미사용
	 *
	 * @param state Boolean
	 * @see android.app.Notification.BigTextStyle
	 * @see android.support.v4.app.NotificationCompat.BigTextStyle
	 */

	public void setUseBigText (Boolean state) {
		mPrefs.putString(PREF_USE_BIGTEXT, state ? "Y" : "N");
	}

	/**
	 * set inbox activity
	 * 
	 * @param inboxActivity
	 */
	public void setInboxActivity (String inboxActivity) {
		mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
	}

	/**
	 * set push popup activity
	 * 
	 * @param classPath
	 */
	public void setPushPopupActivity (String classPath) {
		mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
	}

	/**
	 * set pushPopup showing time
	 * 
	 * @param pushPopupShowingTime
	 */
	public void setPushPopupShowingTime (int pushPopupShowingTime) {
		mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
	}

	/**
	 * get msg flag
	 * 
	 * @return
	 */
	public String getMsgFlag () {
		return mPrefs.getString(PREF_MSG_FLAG);
	}

	/**
	 * get noti flag
	 * 
	 * @return
	 */
	public String getNotiFlag () {
		return mPrefs.getString(PREF_NOTI_FLAG);
	}

	public String getNotiFlag1 () {
		return mPrefs.getString(PREF_NOTI_FLAG1);
	}

	public String getNotiFlag2 () {
		return mPrefs.getString(PREF_NOTI_FLAG2);
	}

	public String getNotiFlag3 () {
		return mPrefs.getString(PREF_NOTI_FLAG3);
	}

	public String getNotiFlag4 () {
		return mPrefs.getString(PREF_NOTI_FLAG4);
	}

	public String getNotiFlag5 () {
		return mPrefs.getString(PREF_NOTI_FLAG5);
	}

	public String getNotiFlag6 () {
		return mPrefs.getString(PREF_NOTI_FLAG6);
	}

	/**
	 * get mkt flag
	 * 
	 * @return
	 */
	public String getMktFlag () {
		return mPrefs.getString(PREF_MKT_FLAG);
	}

	public int getUnReadMsgCnt () {
		return mPrefs.getInt(PREF_UNREAD_CNT);
	}

	public int getTotalMsgCnt () {
		return mPrefs.getInt(PREF_TOTAL_CNT);
	}

	/**
	 * set noti icon
	 * 
	 * @param resId
	 */
	public void setNotiIcon (int resId) {
		mPrefs.putInt(PREF_NOTI_ICON, resId);
	}

	/**
	 * set large noti icon
	 * 
	 * @param resId
	 */
	public void setLargeNotiIcon (int resId) {
		mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
	}

	/**
	 * set noti cound
	 * 
	 * @param resId
	 */
	public void setNotiSound (int resId) {
		mPrefs.putInt(PREF_NOTI_SOUND, resId);
	}

	/**
	 * set noti receiver
	 * 
	 * @param intentAction
	 */
	public void setNotiReceiver (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
	}

	/**
	 * set noti receiver class
	 *
	 * @param intentClass
	 */
	public void setNotiReceiverClass(String intentClass) {
		mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentClass);
	}

	/**
	 * set ring mode
	 * 
	 * @param isRingMode
	 */
	public void setRingMode (boolean isRingMode) {
		mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
	}

	/**
	 * set vibe mode
	 * 
	 * @param isVibeMode
	 */
	public void setVibeMode (boolean isVibeMode) {
		mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	/**
	 * set popup noti
	 * 
	 * @param isShowPopup
	 */
	public void setPopupNoti (boolean isShowPopup) {
		mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	/**
	 * set screen wakeup
	 * 
	 * @param isScreenWakeup
	 */
	public void setScreenWakeup (boolean isScreenWakeup) {
		mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
	}

	public OnReceivePushListener getOnReceivePushListener () {
		return onReceivePushListener;
	}

	public void setOnReceivePushListener (OnReceivePushListener onReceivePushListener) {
		this.onReceivePushListener = onReceivePushListener;
	}

	/**
	 * set debugTag
	 * 
	 * @param tagName
	 */
	public void setDebugTAG (String tagName) {
		CLog.setTagName(tagName);
	}

	/**
	 * set debug mode
	 * 
	 * @param debugMode
	 */
	public void setDebugMode (boolean debugMode) {
		CLog.setDebugMode(debugMode);
	}

	public void bindBadge (Context c, int id) {
		Badge.getInstance(c).addBadge(c, id);
	}

	public void bindBadge (TextView badge) {
		Badge.getInstance(badge.getContext()).addBadge(badge);
	}

	/**
	 * show Message Box
	 * 
	 * @param c
	 */
	public void showMsgBox (Context c) {
		showMsgBox(c, null);
	}

	public void showMsgBox (Context c, Bundle extras) {
		CLog.i("showMsgBox");
		if (PhoneState.isAvailablePush()) {
			try {
				Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

				Intent i = new Intent(mContext, inboxActivity);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				if (extras != null) {
					i.putExtras(extras);
				}
				c.startActivity(i);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			// Intent i = new Intent(INBOX_ACTIVITY);
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// if (extras != null) {
			// i.putExtras(extras);
			// }
			// c.startActivity(i);
		}
	}

	public void closeMsgBox (Context c) {
		Intent i = new Intent(RECEIVER_CLOSE_INBOX);
		c.sendBroadcast(i);
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgID
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * select query
	 * 
	 * @param sql
	 * @return
	 */
	public Cursor selectQuery (String sql) {
		return mDB.selectQuery(sql);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */
	
	/*
	 * ===================================================== [start] android O badge =====================================================
	 */

	/**
	 * Set showing badge at Android O
	 * @param isShowBadge
	 */
	public void setShowNotiChannelBadge (boolean isShowBadge) {
		mPrefs.putBoolean(PREF_NCHANNEL_SHOW_BADGE_FLAG, isShowBadge);
	}

	/**
	 * Get showing badge at Android O
	 * @return
	 */
	@TargetApi(Build.VERSION_CODES.O)
	public boolean getShowNotiChannelBadge () {
		int nNotiChannel = mPrefs.getInt(PREF_NCHANNEL_ID);
		nNotiChannel = nNotiChannel == -1 ? 0 : nNotiChannel;

		NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

		// 기존 고객을 위한 구문 이전 고객들은 고정 ID로 채널 생성 하고 있었음.
		NotificationChannel notiChannel = notificationManager.getNotificationChannel(NOTIFICATION_ID + "");
//		CLog.d("notiChannel 1 : " + notiChannel);
		if (notiChannel != null) {
//			CLog.d("notiChannel.canShowBadge() 1 : " + notiChannel.canShowBadge());
			return notiChannel.canShowBadge();
		} 
		
		// 현재 채널에 대한 값 반환
		notiChannel = notificationManager.getNotificationChannel(nNotiChannel + "");
//		CLog.d("notiChannel 2 : " + notiChannel);
		if (notiChannel != null) {
//			CLog.d("notiChannel.canShowBadge() 2 : " + notiChannel.canShowBadge());
			return notiChannel.canShowBadge();
		}
		
		// 위에 해당 사항이 없을 경우 저장해 놓은 값을 사용하여 channel 노출 여부 반환 (default : false)
//		CLog.d("getNotificationChannel 이 없나!!");
		return mPrefs.getBoolean(PREF_NCHANNEL_SHOW_BADGE_FLAG);
	}

	/**
	 * set builder.setNumber at Android O
	 * @param number
	 */
	public void setNotiChannelBadgeNumber(int number) {
		mPrefs.putInt(PREF_NCHANNEL_BADGE_NUMBER, number);
	}

	/**
	 * get builder.setNumber at Android O
	 * @return
	 */
	public int getNotiChannelBadgeNumber() {
		return mPrefs.getInt(PREF_NCHANNEL_BADGE_NUMBER);
	}

	/**
	 * cancel all notification.
	 */
	public void clearAllNotification() {
		NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancelAll();
	}
	
	/*
	 * ===================================================== [end] android O badge =====================================================
	 */
}
